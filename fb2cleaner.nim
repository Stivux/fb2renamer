import os, strutils, strformat, tables, zippy/ziparchives
proc main() =
    let z = ZipArchive()
    for i in walkFiles("*.fb2.zip"):
        open(z, i)
        for name, data in z.contents:
            if (endsWith(name, "fb2") and contains(data.contents, "<coverpage>") and contains(data.contents, "<coverpage>")):
                z.contents[name] = ArchiveEntry(kind: ekFile, contents: fmt"{data.contents[0..find(data.contents, ""<coverpage>"") - 1]}{data.contents[find(data.contents, ""</coverpage>"") + 12..find(data.contents, ""<binary"") - 1]}</FictionBook>")
                writeZipArchive(z, i)
            else:
                echo("В файле '" & i & "' отсутствует обложка, поэтому он пропущен.")
main()
