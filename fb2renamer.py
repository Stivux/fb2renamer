import os
import glob
import zipfile
def main():
    os.chdir(os.path.dirname(__file__))
    fb2zip = [zipfile.ZipFile(i, 'r') for i in glob.glob('*.fb2.zip')]
    for book in fb2zip:
        content = book.open(book.namelist()[0]).read().decode('utf-8')[222:600]
        if '<first-name>' in content and '<last-name>' in content and '<book-title>' in content:
            book.close()
            os.rename(book.filename, f"{content[content.find('<last-name>') + 11: content.find('</last-name>')]} {content[content.find('<first-name>') + 12: content.find('</first-name>')]} – {content[content.find('<book-title>') + 12: content.find('</book-title>')]}.fb2.zip")
        else:
            print(f'Файл {book.filename} поврежден, поэтому он пропущен.')
main()
