### **fbrenamer.py renames .fb2.zip archives**
1. Move .py file to the folder where .fb2.zip archives are located.
2. Run .py file.
3. Done! All fb2.zip archives in this folder  is renaming as "*Last_name First_name – Book_title.fb2.zip*".
> **Example**: "Dostoevsky.fb2.zip" –> "Достоевский Федор – Идиот.fb2.zip"

> **Update:** fb2cleaner.py removes covers from .fb2.zip archives

### **fb2renamer.py переименовывает .fb2.zip архивы**
1. Переместите .py файл в папку, где находятся .fb2.zip архивы.
2. Запустите .py файл .
3. Готово! Все fb2.zip архивы в папке переименованы по типу "*Фамилия Имя – Название_книги.fb2.zip*".
> **Пример**: "Dostoevsky.fb2.zip" –> "Достоевский Федор – Идиот.fb2.zip"

> **Обновлено:** fb2cleaner.py удаляет обложку и все изображения из .fb2.zip архива

Также добавил Nim-версии скриптов
