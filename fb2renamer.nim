import os, strutils, tables, zippy/ziparchives
proc main() =
    let z = ZipArchive()
    for i in walkFiles("*.fb2.zip"):
        open(z, i)
        for name, data in z.contents:
            if (endsWith(name, "fb2") and contains(data.contents, "<first-name>") and contains(data.contents, "<last-name>") and contains(data.contents, "<book-title>")):
                moveFile(i, data.contents[find(data.contents, "<last-name>") + 11..find(data.contents, "</last-name>") - 1] & ' ' & data.contents[find(data.contents, "<first-name>") + 12..find(data.contents, "</first-name>") - 1] & " – " & data.contents[find(data.contents, "<book-title>") + 12..find(data.contents, "</book-title>") - 1] & ".fb2.zip")
            else:
                echo("Файл " & i & " поврежден, поэтому он пропущен." )
main()
