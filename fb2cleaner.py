import os
import glob
import zipfile
def main():
    os.chdir(os.path.dirname(__file__))
    fb2zip = [zipfile.ZipFile(i, 'r') for i in glob.glob('*.fb2.zip')]
    for book in fb2zip:
        content = book.open(book.namelist()[0]).read().decode('utf-8')
        if '<coverpage>' in content and '<binary' in content:
            book.close()
            zipfile.ZipFile(book.filename, 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9).writestr(book.namelist()[0], f"{content[0:content.find('<coverpage>')]}{content[content.find('</coverpage>') + 12:content.find('<binary')]}</FictionBook>", compresslevel=9)
        else:
        	print(f'В файле {book.filename} отсутствует обложка, поэтому он пропущен.')
main()
